import React, { useState, useEffect, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Navbar, Nav, Container } from 'react-bootstrap';
import banner from '../logo.png';
import './AppNavbar.css';
import UserContext from '../AppContext';

const AppNavbar = ({ expanded, isSmallScreen, handleNavbarToggle }) => {
  const { user } = useContext(UserContext);

  const renderNavbar = () => {
    if (user.id !== null) {
      if (user.isAdmin) {
        return (
          <AdminNavbar
            expanded={expanded}
            isSmallScreen={isSmallScreen}
            handleNavbarToggle={handleNavbarToggle}
          />
        );
      } else {
        return (
          <UserNavbar
            expanded={expanded}
            isSmallScreen={isSmallScreen}
            handleNavbarToggle={handleNavbarToggle}
          />
        );
      }
    } else {
      return (
        <GuestNavbar
          expanded={expanded}
          isSmallScreen={isSmallScreen}
          handleNavbarToggle={handleNavbarToggle}
        />
      );
    }
  };

  return renderNavbar();
};

const AdminNavbar = ({ expanded, isSmallScreen, handleNavbarToggle }) => {
  return (
    <Navbar expand="lg" className="fixed-top" expanded={expanded}>
      <Container>
        <Navbar.Brand as={NavLink} to="/adminProducts" className={isSmallScreen ? 'text-center' : 'text-lg-left'}> B V C Admin Dashboard
        </Navbar.Brand>
        <Navbar.Toggle onClick={handleNavbarToggle} />
        <Navbar.Collapse>
          <Nav className="ml-auto text-center">
          <Nav.Link as={NavLink} to="/adminProducts" exact>
              Products
            </Nav.Link>

            <Nav.Link as={NavLink} to="/adminUsers" exact>
              Users
            </Nav.Link>

            <Nav.Link as={NavLink} to="/adminOrders" exact>
              Orders
            </Nav.Link>

            <Nav.Link as={NavLink} to="/logout" exact>
              Logout
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

const UserNavbar = ({ expanded, isSmallScreen, handleNavbarToggle }) => {
  return (
    <Navbar expand="lg" className="fixed-top" expanded={expanded}>
      <Container>
        <Navbar.Brand as={NavLink} to="/" className={isSmallScreen ? 'text-center' : 'text-lg-left'}>
          <img className="banner rounded-circle border border-dark" src={banner} alt="Brew Voyage Logo" /> B V C
        </Navbar.Brand>
        <Navbar.Toggle onClick={handleNavbarToggle} />
        <Navbar.Collapse>
          <Nav className="ml-auto text-center">
            <Nav.Link as={NavLink} to="/products" exact>
              Products
            </Nav.Link>
            <Nav.Link as={NavLink} to="/orders" exact>
              Orders
            </Nav.Link>
            <Nav.Link as={NavLink} to="/carts" exact>
              Add to Cart
            </Nav.Link>
            <Nav.Link as={NavLink} to="/logout" exact>
              Logout
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

const GuestNavbar = ({ expanded, isSmallScreen, handleNavbarToggle }) => {
  return (
    <Navbar expand="lg" className="fixed-top" expanded={expanded}>
      <Container>
        <Navbar.Brand as={NavLink} to="/" className={isSmallScreen ? 'text-center' : 'text-lg-left'}>
          <img className="banner rounded-circle border border-dark" src={banner} alt="Brew Voyage Logo" /> B V C
        </Navbar.Brand>
        <Navbar.Toggle onClick={handleNavbarToggle} />
        <Navbar.Collapse>
          <Nav className="ml-auto text-center">
            <Nav.Link as={NavLink} to="/products" exact>
              Products
            </Nav.Link>
            <Nav.Link as={NavLink} to="/login" exact>
              Login
            </Nav.Link>
            <Nav.Link as={NavLink} to="/register" exact>
              Register
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};


export default AppNavbar;
