import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import { UserProvider } from './AppContext';
import ErrorPage from './pages/ErrorPage';
import ProductsCatalog from './pages/ProductsCatalog';
import AdminProducts from './pages/AdminProducts';
import AdminUsers from './pages/AdminUsers';
import AdminOrders from './pages/AdminOrders';
import Orders from './pages/Orders';
import AddToCart from './pages/AddToCart';



function App() {

    // Global state hook for the user information for validating if a user is logged in
    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

    // Function for clearing localStorage on logout
    const unsetUser = () => {
      localStorage.clear();
    }

    // Used to check if the user info is properly stored upon login and if the localStorage is cleared upon logout
    useEffect(() => {
      console.log(user);
      console.log(localStorage);
    }, [user])

    useEffect(() => {
      fetch("https://e-commerce-api-36l4.onrender.com/details", {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
         
         if(data._id !== undefined) {

            // Sets the use state values with the user details upon successful login
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });

          // Sets the user state to the initial value 
         } else {
            setUser({
                id: null,
                isAdmin: null
            });
         }
          
      });
    }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/products" element={<ProductsCatalog />} />
            <Route path="/adminProducts" element={<AdminProducts />} />
            <Route path="/adminUsers" element={<AdminUsers/>} />
            <Route path="/adminOrders" element={<AdminOrders/>} />
            <Route path="/orders" element={<Orders/>} />
            <Route path="/carts" element={<AddToCart/>} />
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
