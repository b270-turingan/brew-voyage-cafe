import React from 'react';
import { Link } from 'react-router-dom';
import '../App.css';

const NotFound = () => {
  return (
    <div className="not-found">
      <h1>404 Error</h1>
      <p>Oops! The page you're looking for could not be found.</p>
      <p>Go back to <Link to="/" className="text-decoration-none">Home</Link></p>
    </div>
  );
};

export default NotFound;
