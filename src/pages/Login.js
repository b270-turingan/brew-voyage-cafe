import React, { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import UserContext from '../AppContext';
import { MDBRow, MDBCol, MDBInput } from 'mdb-react-ui-kit';

function Login() {
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);

  function authenticate(e) {
    e.preventDefault();

    fetch('https://e-commerce-api-36l4.onrender.com/users/login', {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data.accessToken !== undefined) {
          localStorage.setItem('token', data.accessToken);

          // Check if user is an admin
          fetch('https://e-commerce-api-36l4.onrender.com/users/', {
            headers: {
              Authorization: `Bearer ${data.accessToken}`
            }
          })
            .then(res => res.json())
            .then(userData => { 
              console.log(userData);

              setUser({
                id: userData._id,
                isAdmin: userData.isAdmin
              });

              confirmAlert({
                title: 'Success',
                message: `Welcome Back to Brew Voyage Cafe!`,
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => console.log('OK')
                  }
                ]
              });
            })
            .catch(error => {
              console.error('Error retrieving user details:', error);
              confirmAlert({
                title: 'Error',
                message: 'An error occurred while retrieving user details',
                buttons: [
                  {
                    label: 'OK',
                    onClick: () => console.log('OK')
                  }
                ]
              });
            });
        } else {
          confirmAlert({
            title: 'Error',
            message: 'Authentication Failed. Please check your credentials',
            buttons: [
              {
                label: 'OK',
                onClick: () => console.log('OK')
              }
            ]
          });
        }
      });

    setEmail('');
    setPassword('');
  }

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (

    user.id !== null ? (
      user.isAdmin ? <Navigate to="/adminProducts" /> : <Navigate to="/" />
    ) : (

      <div>
        <MDBRow>
          <MDBCol sm='6'>
            <div className='d-flex flex-column justify-content-center h-custom-2 w-75 pt-4 loginpage'>
              <h1 className="text-center mb-3 ps-5 pb-3">WELCOME BACK!</h1>
              <h3 className="fw-normal mb-3 ps-5 pb-3 text-center" style={{ letterSpacing: '1px' }}>Log in</h3>
              <form onSubmit={authenticate}>
                <MDBInput
                  wrapperClass='mb-4 mx-5 w-100'
                  label='Email address'
                  id='formControlLg'
                  type='email'
                  size="lg"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <MDBInput
                  wrapperClass='mb-4 mx-5 w-100'
                  label='Password'
                  id='formControlLg'
                  type='password'
                  size="lg"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                {isActive ? (
                  <Button className="mb-4 mx-5 w-100" variant="primary" type="submit" id="submitBtn">
                    Submit
                  </Button>
                ) : (
                  <Button className="mb-4 mx-5 w-100" variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                  </Button>
                )}
              </form>
              <p className="small mb-5 pb-lg-3 ms-5 text-center"><a className="text-muted" href="#!">Forgot password?</a></p>
              <p className='ms-5 text-center'>Don't have an account? <a href="register" className="link-info">Register here</a></p>
            </div>
          </MDBCol>
          <MDBCol sm='6' className='d-none d-sm-block px-0'>
            <img src="https://images.unsplash.com/photo-1517487881594-2787fef5ebf7?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8d2hpdGUlMjBjb2ZmZWV8ZW58MHx8MHx8fDA%3D&w=1000&q=80"
              alt="Login image" className="w-100 h-100" style={{ objectFit: 'cover', objectPosition: 'left' }} />
          </MDBCol>
        </MDBRow>
      </div>
    )
  );
}

export default Login;
