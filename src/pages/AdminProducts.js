import { useContext, useEffect, useState } from "react";
import { Button, Table, Modal, Form, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import UserContext from "../AppContext";
import "../App.css";

export default function AdminDashboard() {

  const { user } = useContext(UserContext);

  const [allProducts, setAllProducts] = useState([]);
  const [productId, setProductId] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [sold, setSold] = useState(0);
  const [type, setType] = useState("");
  const [isActive, setIsActive] = useState(false);
  const [showAdd, setShowAdd] = useState(false);
  const [showEdit, setShowEdit] = useState(false);

  const openAdd = () => setShowAdd(true);
  const closeAdd = () => setShowAdd(false);

  const openEdit = (id) => {
    setProductId(id);

    fetch(`https://e-commerce-api-36l4.onrender.com/products/edit/${id}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });

    setShowEdit(true);
  };

  const closeEdit = () => {
    setName("");
    setDescription("");
    setPrice(0);

    setShowEdit(false);
  };

  const fetchData = () => {
    fetch(`https://e-commerce-api-36l4.onrender.com/products/all`, {
      headers: {
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setAllProducts(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const archive = (id, name) => {
    confirmAlert({
      title: 'Confirm Archive',
      message: `Are you sure you want to archive ${name}?`,
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            fetch(`https://e-commerce-api-36l4.onrender.com/products/archive/${id}`, {
              method: "PATCH",
              headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
              },
              body: JSON.stringify({ isActive: false })
            })
              .then(res => res.json())
              .then(data => {
                console.log(data);
                fetchData();
              });
          }
        },
        {
          label: 'No'
        }
      ]
    });
  };

  const unarchive = (id, name) => {
    confirmAlert({
      title: 'Confirm Unarchive',
      message: `Are you sure you want to unarchive ${name}?`,
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            fetch(`https://e-commerce-api-36l4.onrender.com/products/archive/${id}`, {
              method: "PATCH",
              headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
              },
              body: JSON.stringify({ isActive: true })
            })
              .then(res => res.json())
              .then(data => {
                console.log(data);
                fetchData();
              });
          }
        },
        {
          label: 'No'
        }
      ]
    });
  };


  const addProduct = () => {
    const newProduct = {
      type,
      name,
      description,
      price,
      sold,
      isActive: true
    };

    fetch(`https://e-commerce-api-36l4.onrender.com/products/newProduct`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify(newProduct)
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        closeAdd();
        fetchData();
      });
  };

  const updateProduct = () => {
    const updatedProduct = {
      type,
      name,
      description,
      price
    };

    fetch(`https://e-commerce-api-36l4.onrender.com/products/edit/${productId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify(updatedProduct)
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        closeEdit();
        fetchData();
      });
  };

  const renderProducts = () => {
    return allProducts.map((product) => {
      return (
        <tr key={product._id}>
          <td>{product.type}</td>
          <td>{product.name}</td>
          <td>{product.description}</td>
          <td>{product.price}</td>
          <td>{product.sold}</td>
          <td>{product.isActive ? 'Active' : 'Inactive'}</td>
          <td>
            {product.isActive ? (
              <Button variant="danger" size="sm" onClick={() => archive(product._id, product.name)}>
                Archive
              </Button>
            ) : (
              <>
                <Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>
                  Unarchive
                </Button>
                <Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>
                  Edit
                </Button>
              </>
            )}
          </td>
        </tr>
      );
    });
  };

  return (
    <>
      {!user ? (
        <Navigate to="/login" />
      ) : (
        <>
          <Container className="container-dashboard">
            <h2 className="text-center">BVC PRODUCTS</h2>

            <Button variant="primary" onClick={openAdd} style={{ width: '150px', backgroundColor: 'brown', borderColor: 'black', color: '#ffffff' }}>
              Add Product
            </Button>


            <Table striped bordered hover className="product-table mt-4">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Sold</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>{renderProducts()}</tbody>
            </Table>
          </Container>

          <Modal show={showAdd} onHide={closeAdd} centered>
            <Modal.Header closeButton>
              <Modal.Title className="text-primary">Add Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>

              <Form.Group controlId="formType">
                <Form.Label>Type</Form.Label>
                <Form.Select value={type} onChange={(e) => setType(e.target.value)}>
                  <option value="">Select Type</option>
                  <option value="Snacks">Snacks</option>
                  <option value="Fruit Tea">Fruit Tea</option>
                  <option value="Coffee">Coffee</option>
                </Form.Select>
              </Form.Group>

                <Form.Group controlId="formName">
                  <Form.Label>Name</Form.Label>
                  <Form.Control type="text" placeholder="Enter name" value={name} onChange={(e) => setName(e.target.value)} />
                </Form.Group>

                <Form.Group controlId="formDescription">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    placeholder="Enter description"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </Form.Group>

                <Form.Group controlId="formPrice">
                  <Form.Label>Price</Form.Label>
                  <Form.Control type="number" placeholder="Enter price" value={price} onChange={(e) => setPrice(e.target.value)} />
                </Form.Group>

              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={closeAdd}>
                Close
              </Button>
              <Button variant="primary" onClick={addProduct}>
                Add
              </Button>
            </Modal.Footer>
          </Modal>

          <Modal show={showEdit} onHide={closeEdit} centered>
            <Modal.Header closeButton>
              <Modal.Title className="text-info">Edit Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="formName">
                  <Form.Label>Name</Form.Label>
                  <Form.Control type="text" placeholder="Enter name" value={name} onChange={(e) => setName(e.target.value)} />
                </Form.Group>

                <Form.Group controlId="formDescription">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    placeholder="Enter description"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </Form.Group>

                <Form.Group controlId="formPrice">
                  <Form.Label>Price</Form.Label>
                  <Form.Control type="number" placeholder="Enter price" value={price} onChange={(e) => setPrice(e.target.value)} />
                </Form.Group>

                <Form.Group controlId="formType">
                  <Form.Label>Type</Form.Label>
                  <Form.Select value={type} onChange={(e) => setType(e.target.value)}>
                    <option value="">Select Type</option>
                    <option value="Snacks">Snacks</option>
                    <option value="Fruit Tea">Fruit Tea</option>
                    <option value="Coffee">Coffee</option>
                  </Form.Select>
                </Form.Group>

              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={closeEdit}>
                Close
              </Button>
              <Button variant="primary" onClick={updateProduct}>
                Update
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      )}
    </>
  );
};


